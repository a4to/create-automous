#!/usr/bin/env node

const {execSync} = require('child_process');
const path = require('path');
const fs = require('fs');
require('axioms.js')();

const appName = () => process.argv[2] == undefined ? process.argv[2] = 'Automous_Chatbot' : process.argv[2];

const runCmd =  (cmd) => {
  try { execSync(cmd, {stdio: 'inherit'});
  } catch (e) {
    console.error(`Error - Failed to execute: ${cmd}`, e);
    return false;
  };
  return true;
}

const installDepsCmd = `cd ${appName()} && npx yarn`;
const startAutomous = `cd ${appName()} && npx yarn start`;
const gitCheckoutCmd = `git clone --depth 1 -b base https://gitlab.com/a4to/automous-framework.git ${appName()}`;

const getLatestVersion = (app) => {
  const version = execSync(`npm show ${app} version`).toString().trim();
  return version;
}

const packageJson = `{
  "name": "${appName()}",
  "version": "0.1.0",
  "description": "Automous Chatbot Setup",
  "license": "MIT",
  "main": "./lib/serve.js",
  "scripts": {
    "start": "node ./lib/serve.js",
    "serve": "npx serve -l 3000 -c src",
    "setup": "node ./lib/setBot.js"
  }
}`;


log(`:g:[+] :x:Creating Automous Application Base :c:...`);
const checkedOut = runCmd(gitCheckoutCmd);
const makePackageJson = fs.writeFileSync(`${appName()}/package.json`, packageJson);
if (!checkedOut || !packageJson) process.exit(1);

log(`:y:[?] :x:Installing dependencies :c:...`);
const installDeps = runCmd(installDepsCmd);
if (!installDeps) process.exit(1);

log(`:g:[+] :x:Setup Complete!:c:`);
log(`:y:Starting Server:c:`);
const started = runCmd(startAutomous);
if (!started) process.exit(1);

log(`:g:\nThank you for using Automous!:c:`);


